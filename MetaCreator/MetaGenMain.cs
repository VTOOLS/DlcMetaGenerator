﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;
using MetaCreator.Utils;
namespace MetaCreator
{
    public partial class MetaGenMain : Form
    {
        private int expandHeight = 35;

        public MetaGenMain()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            panel1.Controls.Add(gen_Button);
            gen_Button.Dock = DockStyle.Right;

            panel2.Controls.Add(addYtyp_button);
            panel2.Controls.Add(addRPF_button);
            panel2.Controls.Add(name_TXT);
            panel2.Controls.Add(name_TB);
        }

        private void addRPF_button_Click(object sender, EventArgs e)
        {
            string name = Interaction.InputBox("Rpf name?", "Name?", "");
            string location = "";
            if(name != "")
            {
                location = Interaction.InputBox("Rpf directory?", "Location?", "/%PLATFORM%/levels/gta5/" + name + ".rpf");
            }
            AddInfo(name, location, "Rpf:");
        }

        private void addYtyp_button_Click(object sender, EventArgs e)
        {
            string name = Interaction.InputBox("Ytyp name?", "Name?", "");
            string location = "";
            if (name != "")
            {
                location = Interaction.InputBox("Ytyp directory?", "Location?", "/%PLATFORM%/levels/gta5/" + name + ".ityp");
            }
            AddInfo(name, location, "Ytyp:");
        }

        private void AddInfo(string Name, string Location, string fileType)
        {
            if (Name == "") { MessageBox.Show("Must Enter in " + fileType + " name!", "Error!"); return; }
            if (Location == "") { MessageBox.Show("Must Enter in " + fileType + " Directory!", "Error!"); return; }

            FileInfo fi = new FileInfo();

            fi.Name_TXT.Text = Name;
            fi.Location_TXT.Text = Location;
            fi.fileType.Text = fileType;
            fileInfo_Panel.Controls.Add(fi);

            fileInfo_Panel.Height += expandHeight;

            if (this.Height < 500)
            {
                this.Height += expandHeight;
            }
        }

        private void gen_Button_Click(object sender, EventArgs e)
        {
            GenerateFiles();
        }

        private void GenerateFiles()
        {
            List<FileInfo> fileInfoList = new List<FileInfo>();

            if (fileInfo_Panel.Controls.Count > 0)
            {
                //foreach (FileInfo fi in fileInfo_Panel.Controls)
                //{
                //    if (fi.fileType.Text.ToLower() == "rpf:")
                //    {
                //        MessageBox.Show(fi.Name_TXT.Text);
                //    }
                //    if (fi.fileType.Text.ToLower() == "ytyp:")
                //    {
                //        MessageBox.Show(fi.Name_TXT.Text);
                //    }
                //}

                foreach(FileInfo fi in fileInfo_Panel.Controls)
                {
                    fileInfoList.Add(fi);
                }

                GenerateMetaData.GenerateContentXML(name_TB.Text, fileInfoList, Application.StartupPath);
                GenerateMetaData.GenerateSetup2XML(name_TB.Text, Application.StartupPath);

                MessageBox.Show("Your Content.xml and Setup2.xml was generated, look in application startup path for them!", "Completed", MessageBoxButtons.OK);
            }
            else
                MessageBox.Show("Cant create metadata without rpfs or ytyps!");
        }


    }
}
