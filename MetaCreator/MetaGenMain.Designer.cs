﻿namespace MetaCreator
{
    partial class MetaGenMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MetaGenMain));
            this.addRPF_button = new System.Windows.Forms.Button();
            this.addYtyp_button = new System.Windows.Forms.Button();
            this.gen_Button = new System.Windows.Forms.Button();
            this.name_TB = new System.Windows.Forms.TextBox();
            this.name_TXT = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.fileInfo_Panel = new System.Windows.Forms.FlowLayoutPanel();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // addRPF_button
            // 
            this.addRPF_button.Location = new System.Drawing.Point(216, 38);
            this.addRPF_button.Name = "addRPF_button";
            this.addRPF_button.Size = new System.Drawing.Size(75, 23);
            this.addRPF_button.TabIndex = 0;
            this.addRPF_button.Text = "Add RPF";
            this.addRPF_button.UseVisualStyleBackColor = true;
            this.addRPF_button.Click += new System.EventHandler(this.addRPF_button_Click);
            // 
            // addYtyp_button
            // 
            this.addYtyp_button.Location = new System.Drawing.Point(123, 38);
            this.addYtyp_button.Name = "addYtyp_button";
            this.addYtyp_button.Size = new System.Drawing.Size(75, 23);
            this.addYtyp_button.TabIndex = 1;
            this.addYtyp_button.Text = "Add YTYP";
            this.addYtyp_button.UseVisualStyleBackColor = true;
            this.addYtyp_button.Click += new System.EventHandler(this.addYtyp_button_Click);
            // 
            // gen_Button
            // 
            this.gen_Button.Location = new System.Drawing.Point(324, 4);
            this.gen_Button.Name = "gen_Button";
            this.gen_Button.Size = new System.Drawing.Size(75, 23);
            this.gen_Button.TabIndex = 2;
            this.gen_Button.Text = "Generate";
            this.gen_Button.UseVisualStyleBackColor = true;
            this.gen_Button.Click += new System.EventHandler(this.gen_Button_Click);
            // 
            // name_TB
            // 
            this.name_TB.Location = new System.Drawing.Point(89, 12);
            this.name_TB.Name = "name_TB";
            this.name_TB.Size = new System.Drawing.Size(244, 20);
            this.name_TB.TabIndex = 3;
            // 
            // name_TXT
            // 
            this.name_TXT.AutoSize = true;
            this.name_TXT.Location = new System.Drawing.Point(12, 15);
            this.name_TXT.Name = "name_TXT";
            this.name_TXT.Size = new System.Drawing.Size(71, 15);
            this.name_TXT.TabIndex = 4;
            this.name_TXT.Text = "DLC Name:";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.gen_Button);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 101);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(411, 30);
            this.panel1.TabIndex = 5;
            // 
            // panel2
            // 
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(411, 61);
            this.panel2.TabIndex = 6;
            // 
            // fileInfo_Panel
            // 
            this.fileInfo_Panel.Location = new System.Drawing.Point(0, 61);
            this.fileInfo_Panel.Name = "fileInfo_Panel";
            this.fileInfo_Panel.Size = new System.Drawing.Size(411, 40);
            this.fileInfo_Panel.TabIndex = 7;
            // 
            // MetaGenMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(411, 131);
            this.Controls.Add(this.fileInfo_Panel);
            this.Controls.Add(this.name_TXT);
            this.Controls.Add(this.name_TB);
            this.Controls.Add(this.addYtyp_button);
            this.Controls.Add(this.addRPF_button);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(427, 170);
            this.Name = "MetaGenMain";
            this.Text = "Dlc Meta Generator";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button addRPF_button;
        private System.Windows.Forms.Button addYtyp_button;
        private System.Windows.Forms.Button gen_Button;
        private System.Windows.Forms.TextBox name_TB;
        private System.Windows.Forms.Label name_TXT;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.FlowLayoutPanel fileInfo_Panel;
    }
}

