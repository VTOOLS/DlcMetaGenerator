﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml;
using System.Windows.Forms;

namespace MetaCreator.Utils
{
    public static class GenerateMetaData
    {
        public static void GenerateContentXML(string DlcName, List<DataGridViewRow> Fil, string savePath)
        {

            if (File.Exists(savePath))
            {
                if (MessageBox.Show("The file " + savePath + " already exists. Do you want to overwrite it?", "Confirm file overwrite", MessageBoxButtons.YesNo) != DialogResult.Yes)
                {
                    return; //skip this one...
                }
            }

            StringBuilder sb = new StringBuilder();

            sb.AppendLine("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            sb.AppendLine("<CDataFileMgr ContentsOfDataFileXml>");
            sb.AppendLine(" <disabledFiles />");
            sb.AppendLine(" <includedXmlFiles />");
            sb.AppendLine(" <dataFiles>");
            if(Fil.Count == 0)
            {
                return;
            }
            foreach (DataGridViewRow r in Fil)
            {
                if (r.Cells[0].Value == "RPF")
                {
                    sb.AppendLine(" <Item>");
                    sb.AppendLine("     <filename>dlc_" + DlcName + ":" + r.Cells[3].Value + "</filename>");
                    sb.AppendLine("     <fileType>RPF_FILE</fileType>");
                    sb.AppendLine("     <overlay value=\"true\" />");
                    sb.AppendLine("     <disabled value=\"true\" />");
                    sb.AppendLine("     <persistent value=\"true\" />");
                    sb.AppendLine(" </Item>");
                }
                if (r.Cells[0].Value == "YTYP")
                {
                    sb.AppendLine(" <Item>");
                    sb.AppendLine("     <filename>dlc_" + DlcName + ":" + r.Cells[3].Value + "</filename>");
                    sb.AppendLine("     <fileType>DLC_ITYP_REQUEST</fileType>");
                    sb.AppendLine("     <overlay value=\"false\" />");
                    sb.AppendLine("     <disabled value=\"false\" />");
                    sb.AppendLine("     <persistent value=\"false\" />");
                    sb.AppendLine("     <contents>CONTENTS_PROPS</contents>");
                    sb.AppendLine(" </Item>");
                }
                if (r.Cells[0].Value == "VehicleData")
                {
                    sb.AppendLine(" <Item>");
                    sb.AppendLine("     <filename>dlc_" + DlcName + ":" + r.Cells[3].Value + "</filename>");
                    sb.AppendLine("     <fileType>VEHICLE_METADATA_FILE</fileType>");
                    sb.AppendLine("     <overlay value=\"true\" />");
                    sb.AppendLine("     <disabled value=\"true\" />");
                    sb.AppendLine("     <persistent value=\"false\" />");
                    sb.AppendLine(" </Item>");
                }
                if (r.Cells[0].Value == "VehicleVariation")
                {
                    sb.AppendLine(" <Item>");
                    sb.AppendLine("     <filename>dlc_" + DlcName + ":" + r.Cells[3].Value + "</filename>");
                    sb.AppendLine("     <fileType>VEHICLE_VARIATION_FILE</fileType>");
                    sb.AppendLine("     <overlay value=\"false\" />");
                    sb.AppendLine("     <disabled value=\"true\" />");
                    sb.AppendLine("     <persistent value=\"false\" />");
                    sb.AppendLine(" </Item>");
                }
                if (r.Cells[0].Value == "Carcol")
                {
                    sb.AppendLine(" <Item>");
                    sb.AppendLine("     <filename>dlc_" + DlcName + ":" + r.Cells[3].Value + "</filename>");
                    sb.AppendLine("     <fileType>CARCOLS_FILE</fileType>");
                    sb.AppendLine("     <overlay value=\"false\" />");
                    sb.AppendLine("     <disabled value=\"true\" />");
                    sb.AppendLine("     <persistent value=\"false\" />");
                    sb.AppendLine(" </Item>");
                }
                if (r.Cells[0].Value == "Text")
                {
                    sb.AppendLine(" <Item>");
                    sb.AppendLine("     <filename>dlc_" + DlcName + ":" + r.Cells[3].Value + "</filename>");
                    sb.AppendLine("     <fileType>TEXTFILE_METAFILE</fileType>");
                    sb.AppendLine("     <overlay value=\"false\" />");
                    sb.AppendLine("     <disabled value=\"true\" />");
                    sb.AppendLine("     <persistent value=\"true\" />");
                    sb.AppendLine(" </Item>");
                }
                if (r.Cells[0].Value == "VehicleHandling")
                {
                    sb.AppendLine(" <Item>");
                    sb.AppendLine("     <filename>dlc_" + DlcName + ":" + r.Cells[3].Value + "</filename>");
                    sb.AppendLine("     <fileType>HANDLING_FILE</fileType>");
                    sb.AppendLine("     <overlay value=\"false\" />");
                    sb.AppendLine("     <disabled value=\"true\" />");
                    sb.AppendLine("     <persistent value=\"false\" />");
                    sb.AppendLine(" </Item>");
                }
                if (r.Cells[0].Value == "PedData")
                {
                    sb.AppendLine(" <Item>");
                    sb.AppendLine("     <filename>dlc_" + DlcName + ":" + r.Cells[3].Value + "</filename>");
                    sb.AppendLine("     <fileType>PED_METADATA_FILE</fileType>");
                    sb.AppendLine("     <overlay value=\"false\" />");
                    sb.AppendLine("     <disabled value=\"true\" />");
                    sb.AppendLine("     <persistent value=\"false\" />");
                    sb.AppendLine(" </Item>");
                }
                if (r.Cells[0].Value == "VehicleLayout")
                {
                    sb.AppendLine(" <Item>");
                    sb.AppendLine("     <filename>dlc_" + DlcName + ":" + r.Cells[3].Value + "</filename>");
                    sb.AppendLine("     <fileType>VEHICLE_LAYOUTS_FILE</fileType>");
                    sb.AppendLine("     <overlay value=\"false\" />");
                    sb.AppendLine("     <disabled value=\"true\" />");
                    sb.AppendLine("     <persistent value=\"false\" />");
                    sb.AppendLine(" </Item>");
                }
                if (r.Cells[0].Value == "WeaponInfo")
                {
                    sb.AppendLine(" <Item>");
                    sb.AppendLine("     <filename>dlc_" + DlcName + ":" + r.Cells[3].Value + "</filename>");
                    sb.AppendLine("     <fileType>WEAPONINFO_FILE</fileType>");
                    sb.AppendLine("     <overlay value=\"false\" />");
                    sb.AppendLine("     <disabled value=\"true\" />");
                    sb.AppendLine("     <persistent value=\"false\" />");
                    sb.AppendLine(" </Item>");
                }
                if (r.Cells[0].Value == "MetaDataRPF")
                {
                    sb.AppendLine(" <Item>");
                    sb.AppendLine("     <filename>dlc_" + DlcName + ":" + r.Cells[3].Value + "</filename>");
                    sb.AppendLine("     <fileType>WEAPONINFO_FILE</fileType>");
                    sb.AppendLine("     <overlay value=\"false\" />");
                    sb.AppendLine("     <disabled value=\"true\" />");
                    sb.AppendLine("     <persistent value=\"true\" />");
                    sb.AppendLine("     <contents>CONTENTS_DLC_MAP_DATA</contents>");
                    sb.AppendLine(" </Item>");
                }
            }

            sb.AppendLine(" </dataFiles>");
            sb.AppendLine(" <contentChangeSets>");
            sb.AppendLine("     <Item>");
            sb.AppendLine("         <changeSetName>CCS_" + DlcName +"_NG_STREAMING_MAP</changeSetName>");
            sb.AppendLine("         <filesToEnable>");
            foreach(DataGridViewRow r in Fil)
            {
                if (r.Cells[0].Value == "MetaDataRPF")
                {
                    sb.AppendLine("             <Item>dlc_" + DlcName + ":" + r.Cells[3].Value + "</Item>");
                }
               // else
                //{
                    if (r.Cells[0].Value == "VehicleData")
                    {
                        sb.AppendLine("             <Item>dlc_" + DlcName + ":" + r.Cells[3].Value + "</Item>");
                    }
                    if (r.Cells[0].Value == "VehicleVariation")
                    {
                        sb.AppendLine("             <Item>dlc_" + DlcName + ":" + r.Cells[3].Value + "</Item>");
                    }
                    if (r.Cells[0].Value == "Carcol")
                    {
                        sb.AppendLine("             <Item>dlc_" + DlcName + ":" + r.Cells[3].Value + "</Item>");
                    }
                    if (r.Cells[0].Value == "Text")
                    {
                        sb.AppendLine("             <Item>dlc_" + DlcName + ":" + r.Cells[3].Value + "</Item>");
                    }
                    if (r.Cells[0].Value == "VehicleHandling")
                    {
                        sb.AppendLine("             <Item>dlc_" + DlcName + ":" + r.Cells[3].Value + "</Item>");
                    }
                    if (r.Cells[0].Value == "VehicleLayout")
                    {
                        sb.AppendLine("             <Item>dlc_" + DlcName + ":" + r.Cells[3].Value + "</Item>");
                    }
                    if (r.Cells[0].Value == "WeaponInfo")
                    {
                        sb.AppendLine("             <Item>dlc_" + DlcName + ":" + r.Cells[3].Value + "</Item>");
                    }
               // }
                if (r.Cells[0].Value == "RPF")
                {
                    sb.AppendLine("             <Item>dlc_" + DlcName + ":" + r.Cells[3].Value + "</Item>");
                }
                if (r.Cells[0].Value == "YTYP")
                {
                    sb.AppendLine("             <Item>dlc_" + DlcName + ":" + r.Cells[3].Value + "</Item>");
                }
            }
            sb.AppendLine("         </filesToEnable>");
            sb.AppendLine("         <executionConditions>");
            sb.AppendLine("             <activeChangesetConditions>");
            sb.AppendLine("             </activeChangesetConditions>");
            sb.AppendLine("             <genericConditions>$level=MO_JIM_L11</genericConditions>");
            sb.AppendLine("         </executionConditions>");
            sb.AppendLine("     </Item>");
            sb.AppendLine(" </contentChangeSets>");
            sb.AppendLine(" <patchFiles />");
            sb.AppendLine(" <ExportedBY>Skylumz Dlc Meta Creator</ExportedBY>");
            sb.AppendLine("</CDataFileMgr  ContentsOfDataFileXml>");

            File.WriteAllText(savePath, sb.ToString());

        }

        public static void GenerateSetup2XML(string DlcName, string savePath)
        {
            var Timestamp = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();

            if (File.Exists(savePath))
            {
                if (MessageBox.Show("The file " + savePath + " already exists. Do you want to overwrite it?", "Confirm file overwrite", MessageBoxButtons.YesNo) != DialogResult.Yes)
                {
                    return; //skip this one...
                }
            }

            StringBuilder sb = new StringBuilder();

            sb.AppendLine("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            sb.AppendLine("<SSetupData>");
            sb.AppendLine(" <deviceName>dlc_" + DlcName + "</deviceName>");
            sb.AppendLine(" <datFile>content.xml</datFile>");
            sb.AppendLine(" <timeStamp>" + Timestamp + "</timeStamp>");
            sb.AppendLine(" <nameHash>" + DlcName + "</nameHash>");
            sb.AppendLine(" <contentChangeSets />");
            sb.AppendLine(" <contentChangeSetGroups>");

            sb.AppendLine("     <Item>");
            sb.AppendLine("         <NameHash>GROUP_EARLY_ON</NameHash>");
            sb.AppendLine("         <ContentChangeSets>");
            sb.AppendLine("             <Item>CCS_" + DlcName + "_NG_INIT</Item>");
            sb.AppendLine("         </ContentChangeSets>");
            sb.AppendLine("     </Item>");

            sb.AppendLine("     <Item>");
            sb.AppendLine("         <NameHash>GROUP_TITLEUPDATE_STARTUP</NameHash>");
            sb.AppendLine("         <ContentChangeSets />");
            sb.AppendLine("     </Item>");

            sb.AppendLine("     <Item>");
            sb.AppendLine("         <NameHash>GROUP_UPDATE_STREAMING</NameHash>");
            sb.AppendLine("         <ContentChangeSets>");
            sb.AppendLine("             <Item>CCS_" + DlcName + "_NG_STREAMING</Item>");
            sb.AppendLine("             <Item>CCS_" + DlcName + "_NG_STREAMING_MAP</Item>");
            sb.AppendLine("         </ContentChangeSets>");
            sb.AppendLine("     </Item>");

            sb.AppendLine("     <Item>");
            sb.AppendLine("         <NameHash>GROUP_UPDATE_TEXT</NameHash>");
            sb.AppendLine("         <ContentChangeSets>");
            sb.AppendLine("             <Item>CCS_" + DlcName + "_NG_TEXT</Item>");
            sb.AppendLine("         </ContentChangeSets>");
            sb.AppendLine("     </Item>");

            sb.AppendLine("     <Item>");
            sb.AppendLine("         <NameHash>GROUP_UPDATE_DLC_PATCH</NameHash>");
            sb.AppendLine("         <ContentChangeSets />");
            sb.AppendLine("     </Item>");

            sb.AppendLine("     <Item>");
            sb.AppendLine("         <NameHash>GROUP_UPDATE_DLC_METADATA</NameHash>");
            sb.AppendLine("         <ContentChangeSets />");
            sb.AppendLine("     </Item>");

            sb.AppendLine("     <Item>");
            sb.AppendLine("         <NameHash>GROUP_UPDATE_WEAPON_PATCH</NameHash>");
            sb.AppendLine("         <ContentChangeSets />");
            sb.AppendLine("     </Item>");

            sb.AppendLine("     <Item>");
            sb.AppendLine("         <NameHash>GROUP_STARTUP</NameHash>");
            sb.AppendLine("         <ContentChangeSets />");
            sb.AppendLine("     </Item>");

            sb.AppendLine("     <Item>");
            sb.AppendLine("         <NameHash>GROUP_ON_DEMAND</NameHash>");
            sb.AppendLine("         <ContentChangeSets />");
            sb.AppendLine("     </Item>");

            sb.AppendLine(" </contentChangeSetGroups>");
            sb.AppendLine(" <startupScript />");
            sb.AppendLine(" <scriptCallstackSize value=\"0\" />");
            sb.AppendLine(" <type>EXTRACONTENT_LEVEL_PACK</type>");
            sb.AppendLine(" <order value=\"2\" />");
            sb.AppendLine(" <minorOrder value=\"0\" />");
            sb.AppendLine(" <isLevelPack value=\"true\" />");
            sb.AppendLine(" <dependencyPackHash />");
            sb.AppendLine(" <requiredVersion />");
            sb.AppendLine(" <subPackCount value=\"0\" />");
            sb.AppendLine("</SSetupData>");


            File.WriteAllText(savePath, sb.ToString());
        }
    }
}
