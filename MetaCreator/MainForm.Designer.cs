﻿namespace MetaCreator
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.label1 = new System.Windows.Forms.Label();
            this.top_Panel = new System.Windows.Forms.Panel();
            this.addmeta_BTN = new System.Windows.Forms.Button();
            this.filetype_ComboBox = new System.Windows.Forms.ComboBox();
            this.dlcpackname_TB = new System.Windows.Forms.TextBox();
            this.middle_Panel = new System.Windows.Forms.Panel();
            this.metaGrid = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bottom_Panel = new System.Windows.Forms.Panel();
            this.generate_BTN = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.status_TXT = new System.Windows.Forms.ToolStripStatusLabel();
            this.top_Panel.SuspendLayout();
            this.middle_Panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.metaGrid)).BeginInit();
            this.bottom_Panel.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Dlc Pack Name:";
            // 
            // top_Panel
            // 
            this.top_Panel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.top_Panel.Controls.Add(this.addmeta_BTN);
            this.top_Panel.Controls.Add(this.filetype_ComboBox);
            this.top_Panel.Controls.Add(this.dlcpackname_TB);
            this.top_Panel.Controls.Add(this.label1);
            this.top_Panel.Location = new System.Drawing.Point(0, 1);
            this.top_Panel.Name = "top_Panel";
            this.top_Panel.Size = new System.Drawing.Size(569, 78);
            this.top_Panel.TabIndex = 1;
            // 
            // addmeta_BTN
            // 
            this.addmeta_BTN.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.addmeta_BTN.Location = new System.Drawing.Point(491, 51);
            this.addmeta_BTN.Name = "addmeta_BTN";
            this.addmeta_BTN.Size = new System.Drawing.Size(75, 23);
            this.addmeta_BTN.TabIndex = 3;
            this.addmeta_BTN.Text = "Add Meta";
            this.addmeta_BTN.UseVisualStyleBackColor = true;
            this.addmeta_BTN.Click += new System.EventHandler(this.addmeta_BTN_Click);
            // 
            // filetype_ComboBox
            // 
            this.filetype_ComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.filetype_ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.filetype_ComboBox.FormattingEnabled = true;
            this.filetype_ComboBox.Items.AddRange(new object[] {
            "RPF File",
            "MetaDataRPF File",
            "----------------------------------------",
            "YTYP File",
            "----------------------------------------",
            "Text File",
            "----------------------------------------",
            "PEDMetaData File",
            "----------------------------------------",
            "WeaponInfo File",
            "----------------------------------------",
            "VehicleLayout File",
            "VehicleMetaData File",
            "VehicleVariation File",
            "CarCols File",
            "Handling File"});
            this.filetype_ComboBox.Location = new System.Drawing.Point(345, 51);
            this.filetype_ComboBox.Name = "filetype_ComboBox";
            this.filetype_ComboBox.Size = new System.Drawing.Size(140, 21);
            this.filetype_ComboBox.TabIndex = 2;
            // 
            // dlcpackname_TB
            // 
            this.dlcpackname_TB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dlcpackname_TB.Location = new System.Drawing.Point(105, 25);
            this.dlcpackname_TB.Name = "dlcpackname_TB";
            this.dlcpackname_TB.Size = new System.Drawing.Size(461, 20);
            this.dlcpackname_TB.TabIndex = 1;
            // 
            // middle_Panel
            // 
            this.middle_Panel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.middle_Panel.Controls.Add(this.metaGrid);
            this.middle_Panel.Location = new System.Drawing.Point(0, 83);
            this.middle_Panel.Name = "middle_Panel";
            this.middle_Panel.Size = new System.Drawing.Size(569, 232);
            this.middle_Panel.TabIndex = 2;
            // 
            // metaGrid
            // 
            this.metaGrid.AllowUserToAddRows = false;
            this.metaGrid.AllowUserToDeleteRows = false;
            this.metaGrid.AllowUserToResizeRows = false;
            this.metaGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.metaGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column4,
            this.Column2,
            this.Column3});
            this.metaGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metaGrid.Location = new System.Drawing.Point(0, 0);
            this.metaGrid.Name = "metaGrid";
            this.metaGrid.Size = new System.Drawing.Size(569, 232);
            this.metaGrid.TabIndex = 0;
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column1.HeaderText = "MetaType";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "FileType";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column2.HeaderText = "MetaName";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column3.HeaderText = "MetaLocation";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // bottom_Panel
            // 
            this.bottom_Panel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bottom_Panel.Controls.Add(this.generate_BTN);
            this.bottom_Panel.Location = new System.Drawing.Point(0, 315);
            this.bottom_Panel.Name = "bottom_Panel";
            this.bottom_Panel.Size = new System.Drawing.Size(569, 27);
            this.bottom_Panel.TabIndex = 3;
            // 
            // generate_BTN
            // 
            this.generate_BTN.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.generate_BTN.Location = new System.Drawing.Point(481, 3);
            this.generate_BTN.Name = "generate_BTN";
            this.generate_BTN.Size = new System.Drawing.Size(75, 23);
            this.generate_BTN.TabIndex = 0;
            this.generate_BTN.Text = "Generate";
            this.generate_BTN.UseVisualStyleBackColor = true;
            this.generate_BTN.Click += new System.EventHandler(this.generate_BTN_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.status_TXT});
            this.statusStrip1.Location = new System.Drawing.Point(0, 342);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(568, 22);
            this.statusStrip1.TabIndex = 4;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // status_TXT
            // 
            this.status_TXT.Name = "status_TXT";
            this.status_TXT.Size = new System.Drawing.Size(86, 17);
            this.status_TXT.Text = "Meta Creator";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(568, 364);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.bottom_Panel);
            this.Controls.Add(this.middle_Panel);
            this.Controls.Add(this.top_Panel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(235, 392);
            this.Name = "MainForm";
            this.Text = "Meta Generator";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.top_Panel.ResumeLayout(false);
            this.top_Panel.PerformLayout();
            this.middle_Panel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.metaGrid)).EndInit();
            this.bottom_Panel.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel top_Panel;
        private System.Windows.Forms.Button addmeta_BTN;
        private System.Windows.Forms.ComboBox filetype_ComboBox;
        private System.Windows.Forms.TextBox dlcpackname_TB;
        private System.Windows.Forms.Panel middle_Panel;
        private System.Windows.Forms.DataGridView metaGrid;
        private System.Windows.Forms.Panel bottom_Panel;
        private System.Windows.Forms.Button generate_BTN;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel status_TXT;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
    }
}