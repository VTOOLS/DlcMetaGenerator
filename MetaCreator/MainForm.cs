﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetaCreator.Utils;
using System.IO;

namespace MetaCreator
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void AddDeleteButtons()
        {
            var deleteButton = new DataGridViewButtonColumn
            {
                Text = "Delete",
                HeaderText = "Delete",
                UseColumnTextForButtonValue = true,
            };

            metaGrid.Columns.Add(deleteButton);
            metaGrid.CellContentClick += OnCellClick;
        }

        private void OnCellClick(object sender, DataGridViewCellEventArgs e)
        {
            if(e.ColumnIndex == 4)
            {
                var index = e.RowIndex;

                metaGrid.Rows.RemoveAt(e.RowIndex);
                UpdateStatus("Removed Row " + e.RowIndex);
            }
        }

        private void addmeta_BTN_Click(object sender, EventArgs e)
        {
            switch (filetype_ComboBox.Text)
            {
                case "RPF File":
                    AddRPF();
                    break;
                case "YTYP File":
                    AddYTYP();
                    break;
                case "VehicleMetaData File":
                    AddVehicleMetaData();
                    break;
                case "VehicleVariation File":
                    AddVehicleVariation();
                    break;
                case "CarCols File":
                    AddCarCols();
                    break;
                case "Text File":
                    AddText(); 
                    break;
                case "Handling File":
                    AddHandling();
                    break;
                case "PEDMetaData File":
                    AddPEDMetaData();
                    break;
                case "VehicleLayout File":
                    AddVehicleLayout();
                    break;
                case "WeaponInfo File":
                    AddWeaponInfo();
                    break;
                case "MetaDataRPF File":
                    AddMetaDataRPF();
                    break;
                default:
                    MessageBox.Show("Not a supported meta type!", "ERROR");
                    break;
            }
            filetype_ComboBox.Text = "";
        }

        private void AddRPF()
        {
            UpdateStatus("Adding Rpf.....");
            string name = Interaction.InputBox("Rpf name?", "Name?", "");
            string location = "";
            if (name != "")
            {
                location = Interaction.InputBox("Rpf directory?", "Location?", "/%PLATFORM%/levels/gta5/" + name + ".rpf");
            }
            AddFile(name, location, "RPF", ".rpf");
        }

        private void AddYTYP()
        {
            UpdateStatus("Adding YTYP.....");
            string name = Interaction.InputBox("Ytyp name?", "Name?", "");
            string location = "";
            if (name != "")
            {
                location = Interaction.InputBox("Ytyp directory?", "Location?", "/%PLATFORM%/levels/gta5/" + name + ".ityp");
            }
            AddFile(name, location, "YTYP", ".ytyp");
        }

        private void AddVehicleMetaData()
        {
            UpdateStatus("Adding Vehicle Meta Data.....");
            string name = Interaction.InputBox("Vehicle Meta Data name?", "Name?", "");
            string location = "";
            if (name != "")
            {
                location = Interaction.InputBox("Vehicle Meta Data directory?", "Location?", "/%PLATFORM%/levels/gta5/" + name + ".meta");
            }
            AddFile(name, location, "VehicleData", ".meta");
        }

        private void AddVehicleVariation()
        {
            UpdateStatus("Adding VehicleVaraition File.....");
            string name = Interaction.InputBox("VehicleVaraition File name?", "Name?", "");
            string location = "";
            if (name != "")
            {
                location = Interaction.InputBox("VehicleVaraition File directory?", "Location?", "/%PLATFORM%/levels/gta5/" + name + ".meta");
            }
            AddFile(name, location, "VehicleVariation", ".meta");
        }

        private void AddCarCols()
        {
            UpdateStatus("Adding Car Cols File.....");
            string name = Interaction.InputBox("Car Cols File name?", "Name?", "");
            string location = "";
            if (name != "")
            {
                location = Interaction.InputBox("Car Cols File directory?", "Location?", "/%PLATFORM%/levels/gta5/" + name + ".meta");
            }
            AddFile(name, location, "Carcol", ".meta");
        }

        private void AddText()
        {
            UpdateStatus("Adding Text File.....");
            string name = Interaction.InputBox("Text File name?", "Name?", "");
            string location = "";
            if (name != "")
            {
                location = Interaction.InputBox("Text File directory?", "Location?", "/%PLATFORM%/levels/gta5/" + name + ".meta");
            }
            AddFile(name, location, "Text", ".meta");
        }

        private void AddHandling()
        {
            UpdateStatus("Adding Vehicle Handling File.....");
            string name = Interaction.InputBox("Vehicle Handling File name?", "Name?", "");
            string location = "";
            if (name != "")
            {
                location = Interaction.InputBox("Vehicle Handling File directory?", "Location?", "/%PLATFORM%/levels/gta5/" + name + ".meta");
            }
            AddFile(name, location, "VehicleHandling", ".meta");
        }

        private void AddPEDMetaData()
        {
            UpdateStatus("Adding PedMetaData File.....");
            string name = Interaction.InputBox("PedMetaData File name?", "Name?", "");
            string location = "";
            if (name != "")
            {
                location = Interaction.InputBox("PedMetaData File directory?", "Location?", "/%PLATFORM%/levels/gta5/" + name + ".meta");
            }
            AddFile(name, location, "PedData", ".meta");
        }

        private void AddVehicleLayout()
        {
            UpdateStatus("Adding VehicleLayout File.....");
            string name = Interaction.InputBox("VehicleLayout File name?", "Name?", "");
            string location = "";
            if (name != "")
            {
                location = Interaction.InputBox("VehicleLayout File directory?", "Location?", "/%PLATFORM%/levels/gta5/" + name + ".meta");
            }
            AddFile(name, location, "VehicleLayout", ".meta");
        }

        private void AddWeaponInfo()
        {
            UpdateStatus("Adding WeaponInfo File.....");
            string name = Interaction.InputBox("WeaponInfo File name?", "Name?", "");
            string location = "";
            if (name != "")
            {
                location = Interaction.InputBox("WeaponInfo File directory?", "Location?", "/%PLATFORM%/levels/gta5/" + name + ".meta");
            }
            AddFile(name, location, "WeaponInfo", ".meta");
        }

        private void AddMetaDataRPF()
        {
            UpdateStatus("Adding MetaDataRPF File.....");
            string name = Interaction.InputBox("MetaDataRPF File name?", "Name?", "");
            string location = "";
            if (name != "")
            {
                location = Interaction.InputBox("MetaDataRPF File directory?", "Location?", "/%PLATFORM%/levels/gta5/" + name + ".rpf");
            }
            AddFile(name, location, "MetaDataRPF", ".rpf");
        }

        private void AddFile(string Name, string Location, string fileMetaType, string fileType)
        {
            var row = metaGrid.Rows[metaGrid.Rows.Add()];

            row.Cells[0].Value = fileMetaType;

            row.Cells[1].Value = fileType;

            row.Cells[2].Value = Name;

            row.Cells[3].Value = Location;

            UpdateStatus("Added " + Name + fileType);
        }

        private void GenerateMeta()
        {
            UpdateStatus("Generating.....");
            List<DataGridViewRow> dgvr = new List<DataGridViewRow>();
            dgvr.Clear();

            var selectFolderDialog = new FolderBrowserDialog
            {
                Description = "Select a folder to save the files."
            };

            if (selectFolderDialog.ShowDialog() == DialogResult.OK)
            {
                
                foreach (DataGridViewRow row in metaGrid.Rows)
                {
                    dgvr.Add(row);
                }

                var savePath = Path.Combine(selectFolderDialog.SelectedPath, "content.xml");
                GenerateMetaData.GenerateContentXML(dlcpackname_TB.Text, dgvr, savePath);
                savePath = Path.Combine(selectFolderDialog.SelectedPath, "setup2.xml");
                GenerateMetaData.GenerateSetup2XML(dlcpackname_TB.Text, savePath);
            }
            UpdateStatus("Generated you can find the files here: " + selectFolderDialog.SelectedPath);
        }

        private void UpdateStatus(string status)
        {
            status_TXT.Text = status;
        }

        private void generate_BTN_Click(object sender, EventArgs e)
        {
            GenerateMeta();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            AddDeleteButtons();
        }
    }
}
